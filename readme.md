# blanky
Opens outbound links in an new window.<br>

<blockquote>Users … may be search-navigators or link-clickers, but they all have additional mental systems in place that keep them aware of where they are on the site map. That is, if you put the proper markers in place. Without proper beacons to home in on, users will quickly become disoriented.</blockquote>

```
// defaults
$("body").blanky({
    blank: true, 
    noopener: true, 
    nofollow: false, 
    debug: false
});
```

[demo](/src/index.html)



#### Sources and more info
* https://seo-hacker.com/outbound-links-tutorial/
* https://www.caktusgroup.com/blog/2017/03/01/opening-external-links-same-tab-or-new/
* https://medium.com/the-metric/links-should-open-in-the-same-window-447da3ae59ba