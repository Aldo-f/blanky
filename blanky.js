(function ($) {

    $.fn.blanky = function (options) {
        var settings = $.extend({
            // Defaults 
            blank: true,
            noopener: true,
            nofollow: false,
            debug: false
        }, options);

        if (settings.debug) {
            console.log(settings);
        }

        // get current site
        let origin = window.location.origin;
        $(function () {
            // url starts with current site
            checkUrl(origin, settings);
        });
    }

    function checkUrl(origin, settings) {
        $("a").each(function (index, element) {
            let href = $(element).attr('href');

            if (href.indexOf(origin) > -1 || href.startsWith('#')) {
                if (settings.debug) {
                    console.log("internal url:", href);
                }
                // you may set nofollow for internal url's
                noFollow(element, settings);
            } else {
                if (settings.debug) {
                    console.log("external url:", href);
                }
                // set url
                setUrl(element, settings);
            }
        });
    }

    function setUrl(url, settings) {
        blank(url, settings);
        noOpener(url, settings);
        if (!settings.noopener) {
            noFollow(url, settings);
        }
    }

    function blank(url, settings) {
        if (settings.blank) {
            $(url).attr(
                'target', '_blank'
            );
            if (settings.debug) {
                console.log("set _blank");
            }
        }
    }

    function noOpener(url, settings) {

        if (settings.noopener) {
            $(url).attr(
                'rel', 'noopener'
            );
            if (settings.debug) {
                console.log("set noopener");
            }
        }
    }

    function noFollow(url, settings) {

        if (settings.nofollow) {
            $(url).attr(
                'rel', 'nofollow'
            );
            if (settings.debug) {
                console.log("set nofollow");
            }
        }
    }


})(jQuery);